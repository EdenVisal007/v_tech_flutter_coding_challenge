class AppConstant {
  static const String baseURL = "https://";
  static const String appName = "VTech Coding";
  static const String token = 'token';
  static const String logo = 'assets/images/logo.png';
  static const String personList = "personList";
}