class Dimension {
  static double fontSizeDefault = 16;
  static const double paddingSizeExtraExtraSmall = 2.0;
  static const double paddingSizeExtraSmall = 6.0;
  static const double paddingSizeSmall = 10.0;
  static const double homePagePadding = 16.0;
  static const double paddingSizeLarge = 20.0;
  static const double paddingSizeThirty = 30.0;
  static const double paddingSizeOverLarge = 50.0;
  static const double marginSizeSmall = 10.0;
  static const double marginSizeLarge = 20.0;
  static const double marginSizeAuthSmall = 30.0;
  static const double marginSizeAuth = 50.0;
  static const double borderRadius = 4.0;
  static double iconSizeSmall = 24;
  static double iconDefaultSize = 28;
}
