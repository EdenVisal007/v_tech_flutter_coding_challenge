// ignore_for_file: prefer_collection_literals, no_leading_underscores_for_local_identifiers

import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';
import 'package:v_tech_coding/controller/person_controller.dart';
import 'package:v_tech_coding/data/dio/api_client.dart';
import 'package:v_tech_coding/data/repository/person_respository.dart';
import 'package:v_tech_coding/helper/network_info.dart';
import 'package:v_tech_coding/util/app_constant.dart';

Future<Map<String, Map<String, String>>> init() async {

  // Core
  final sharedPreferences = await SharedPreferences.getInstance();
  Get.lazyPut(() => NetworkInfo(Get.find()));
  Get.lazyPut(() => sharedPreferences);
  Get.lazyPut(() => DioClient(appBaseUrl: AppConstant.baseURL, sharedPreferences: Get.find()));

  // Repository
  Get.lazyPut(() => PersonRepository(dioClient: Get.find(), sharedPreferences: Get.find()));

  // Controller
  Get.lazyPut(() => PersonController(personRepository: Get.find(), sharedPreferences: Get.find()));

  return {};
}
