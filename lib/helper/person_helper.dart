//ignore_for_file: avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:v_tech_coding/data/model/person_model.dart';

class PersonHelper {

  final String _person = PersonModel.pPerson;
  Stream<QuerySnapshot<Map<String, dynamic>>> getPersonList() {
    return FirebaseFirestore.instance.collection(_person).snapshots();
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> sortByName() {
    return FirebaseFirestore.instance.collection(_person)
      .orderBy(PersonModel.pName, descending: true).snapshots();
  }

  Future insertPerson(PersonModel person) async {
    return FirebaseFirestore.instance.runTransaction((transaction) async {
      CollectionReference personRef = FirebaseFirestore.instance.collection(_person);
      await personRef.add(person.toMap());
    }).then((_) => print("Person Added")).catchError((error) => print("Failed to add person: $error"));
  }

  Future updatePerson(PersonModel personModel) async {
    return FirebaseFirestore.instance.runTransaction((transaction) async {
      transaction.update(personModel.reference!, personModel.toMap());
    }).then((_) => print("Person Updated")).catchError((error) => print("Failed to update person: $error"));
  }

  Future deletePerson(PersonModel personModel) async {
    return FirebaseFirestore.instance.runTransaction((transaction) async {
      transaction.delete(personModel.reference!);
    }).then((_) => print("Person Deleted")).catchError((error) => print("Failed to delete person: $error"));
  }
}