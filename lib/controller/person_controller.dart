// ignore_for_file: await_only_futures, avoid_print, unnecessary_null_comparison, prefer_const_constructors

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:v_tech_coding/data/repository/person_respository.dart';

class PersonController extends GetxController implements GetxService {

  final PersonRepository personRepository;
  final SharedPreferences sharedPreferences;
  PersonController({required this.personRepository, required this.sharedPreferences});

  // set
  bool _isTypingComplete = false;
  bool _name = false;
  bool _gender = false;
  bool _email = false;
  bool _phone = false;
  bool _skill = false;
  bool _address = false;

  //get
  bool get isTypingComplete => _isTypingComplete;
  bool get name => _name;
  bool get gender => _gender;
  bool get email => _email;
  bool get phone => _phone;
  bool get skill => _skill;
  bool get address => _address;

  setTypingComplete(bool isTypingComplete) {
    _isTypingComplete = isTypingComplete;
    update();
  }

  setName(bool name) {
    _name = name;
    update();
  }

  setGender(bool gender) {
    _gender = gender;
    update();
  }

  setEmail(bool email) {
    _email = email;
    update();
  }

  setPhone(bool phone) {
    _phone = phone;
    update();
  }

  setSkill(bool skill) {
    _skill = skill;
    update();
  }

  setAddress(bool address) {
    _address = address;
    update();
  }
}
