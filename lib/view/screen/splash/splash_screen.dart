// ignore_for_file: prefer_const_constructors, avoid_print

import 'package:flutter/material.dart';
import 'package:v_tech_coding/util/app_constant.dart';
import 'package:v_tech_coding/util/next_screen.dart';
import 'package:v_tech_coding/view/screen/person/person_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  afterSplash() async {
    if(mounted){
      try {
        Future.delayed(Duration(seconds: 2), (){
          nextScreenNoReturn(context, PersonScreen());
        });
      } catch (e) {
        print(e.toString());
      }
    } else{

    }
  }

  @override
  void initState() {
    afterSplash();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange,
      body: Center(
        child: Image(
          width: 110,
          height: 110,
          image: AssetImage(AppConstant.logo),
          fit: BoxFit.contain,
        )
      )
    );
  }
}
