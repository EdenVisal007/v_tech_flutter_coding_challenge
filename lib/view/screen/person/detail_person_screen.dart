// ignore_for_file: prefer_const_constructors, must_be_immutable

import 'package:flutter/material.dart';
import 'package:v_tech_coding/data/model/person_model.dart';

class DetailPersonScreen extends StatefulWidget {

  PersonModel personModel;
  DetailPersonScreen({super.key, required this.personModel});

  @override
  State<DetailPersonScreen> createState() => _DetailPersonScreenState();
}

class _DetailPersonScreenState extends State<DetailPersonScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar,
      body: _buildBody,
    );
  }

  // Todo: buildAppBar
  AppBar get _buildAppBar {
    return AppBar(
      backgroundColor: Colors.orange,
      centerTitle: true,
      title: Text('Detail Person'),
    );
  }

  // Todo: buildBody
  Widget get _buildBody {
    var personModel = widget.personModel;
    return Container(
      alignment: Alignment.topCenter,
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 16),
          CircleAvatar(
            radius: 40,
            backgroundColor: Colors.orange,
            child: Text(
              personModel.name!.substring(0, 1),
              style: TextStyle(fontSize: 26),
            ),
          ),
          SizedBox(height: 16),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildCustomText("Username: ", "${personModel.name}"),
              buildCustomText("Gender: ", "${personModel.gender}"),
              buildCustomText("Email: ", "${personModel.email}"),
              buildCustomText("Phone: ", "${personModel.phone}"),
              buildCustomText("Skill: ", "${personModel.skill}"),
              buildCustomText("Address: ", "${personModel.address}"),
            ],
          )
        ],
      ),
    );
  }

  // Todo: buildCustomText
  Widget buildCustomText(String title, String description) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Text.rich(
        TextSpan(
          children: [
            TextSpan(
              text: title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            ),
            TextSpan(
              text: description,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
          ],
        ),
      )
    );
  }
}
