// ignore_for_file: prefer_const_constructors, avoid_print, prefer_const_literals_to_create_immutables, must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:v_tech_coding/controller/person_controller.dart';
import 'package:v_tech_coding/data/model/person_model.dart';
import 'package:v_tech_coding/helper/person_helper.dart';
import 'package:v_tech_coding/view/custom/custom_button_sheet_content_screen.dart';
import 'package:v_tech_coding/view/custom/custom_button_widget.dart';
import 'package:v_tech_coding/view/custom/custom_textfield_widget.dart';

class CrudPersonScreen extends StatefulWidget {

  bool? isInsert;
  PersonModel? personModel;
  CrudPersonScreen({super.key, this.isInsert = true, this.personModel});

  @override
  State<CrudPersonScreen> createState() => _CrudPersonScreenState();
}

class _CrudPersonScreenState extends State<CrudPersonScreen> {

  final TextEditingController _nameCtrl = TextEditingController();
  final TextEditingController _genderCtrl = TextEditingController();
  final TextEditingController _emailCtrl = TextEditingController();
  final TextEditingController _phoneCtrl = TextEditingController();
  final TextEditingController _skillCtrl = TextEditingController();
  final TextEditingController _addressCtrl = TextEditingController();
  PersonHelper personHelper = PersonHelper();
  PersonController personController = Get.find<PersonController>();

  bool _isLoading = true;
  List skillOther = ["Web Developer", "Android Developer", "iOS Developer", "Flutter Developer"];
  List genderType = ["Male", "Female"];

  _init() async {
    var person = widget.personModel;
    _nameCtrl.text = person?.name ?? "";
    _genderCtrl.text = person?.gender ?? "";
    _emailCtrl.text = person?.email ?? "";
    _phoneCtrl.text = person?.phone ?? "";
    _skillCtrl.text = person?.skill ?? "";
    _addressCtrl.text = person?.address ?? "";
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _init();
    genderNumber = -1;
    skillNumber = -1;
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PersonController>(builder: (personController) {
      return Scaffold(
        appBar: _buildAppBar(personController),
        body: Stack(
          children: [
            _buildBody(personController),
            _isLoading ? Center(child: CircularProgressIndicator(color: Colors.orange)) : SizedBox(),
          ],
        ),
      );
    });
  }

  // Todo: buildAppBar
  AppBar _buildAppBar(PersonController personController) {
    return AppBar(
      backgroundColor: Colors.orange,
      leading: IconButton(
        onPressed: () => Get.back(),
        icon: ImageIcon(AssetImage("assets/images/arrow_left.png"), color: Colors.white, size: 28),
      ),
      title: Text(
        widget.isInsert == true ? "Insert Person" : "Update Person",
        style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w600)
      ),
      centerTitle: true,
      elevation: 0,
    );
  }

  // Todo: buildBody
  Widget _buildBody(PersonController personController) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Container(
        alignment: Alignment.topCenter,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Wrap(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 12, vertical: 16),
                    decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 16),
                        _buildName(personController),
                        SizedBox(height: 20),
                        _buildGender(personController),
                        SizedBox(height: 20),
                        _buildEmail(personController),
                        SizedBox(height: 20),
                        _buildPhone(personController),
                        SizedBox(height: 20),
                        _buildSkill(personController),
                        SizedBox(height: 20),
                        _buildAddress(personController),
                        SizedBox(height: 20),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 12),
                          child: CustomButtonWidget.buildButtonClick(
                            title: widget.isInsert == true ? "Create Person" : "Update Person",
                            activeColor: true,
                            onPress: (
                              _nameCtrl.text.isEmpty || _genderCtrl.text.isEmpty || (_emailCtrl.text.length < 8) ||
                              _phoneCtrl.text.length < 8 || _skillCtrl.text.isEmpty) ? null : () {
                              if(widget.isInsert == true) {
                                PersonModel personModel = PersonModel(
                                  name: _nameCtrl.text,
                                  gender: _genderCtrl.text,
                                  email: _emailCtrl.text,
                                  phone: _phoneCtrl.text,
                                  skill: _skillCtrl.text,
                                  address: _addressCtrl.text,
                                );
                                personHelper.insertPerson(personModel).then((_) => Get.back());
                              } else {
                                var person = widget.personModel;
                                person?.name = _nameCtrl.text;
                                person?.gender = _genderCtrl.text;
                                person?.email = _emailCtrl.text;
                                person?.phone = _phoneCtrl.text;
                                person?.skill = _skillCtrl.text;
                                person?.address = _addressCtrl.text;
                                personHelper.updatePerson(person!).then((_) => Get.back());
                              }
                              Get.back();
                            }
                          ),
                        ),
                        SizedBox(height: 32),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Todo: buildName
  Widget _buildName(PersonController personController) {
    return Container(
      height: 78,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Wrap(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Username", style: TextStyle(color: Colors.black, fontSize: 16),),
              SizedBox(height: 8,),
              SizedBox(
                height: 46,
                child: CustomTextFieldWidget(
                  _nameCtrl,
                  isPhoneNumber: false,
                  label: "Enter username",
                  personController: personController,
                  onChange: (value){
                    if(_nameCtrl.text.isNotEmpty){
                      personController.setName(true);
                    }else {
                      personController.setName(false);
                    }
                  },
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  // Todo: buildGender
  Widget _buildGender(PersonController personController) {
    return Container(
      height: 78,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Wrap(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Gender", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w400)),
              SizedBox(height: 8),
              CupertinoButton(
                padding: EdgeInsets.all(0),
                onPressed: (){
                  customBuildButtonSheetCustom(
                    context: context,
                    titleName: "Select your gender",
                    selectInformationType: SelectInformationType.gender,
                    allItem: genderType,
                    onText: () {
                      setState(() {
                        _genderCtrl.text = genderType[genderNumber];
                      });
                      Get.back();
                    },
                  );
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 16, right: 8),
                  height: 46,
                  width: double.infinity,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(4), color: Colors.grey.shade200),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        _genderCtrl.text.isEmpty ? "select" : _genderCtrl.text,
                        style: TextStyle(color: _genderCtrl.text.isEmpty ? Colors.grey : Colors.black87)
                      ),
                      Spacer(),
                      Icon(Icons.arrow_drop_down, color: Colors.grey, size: 28)
                    ],
                  ),
                )
              )
            ],
          ),
        ],
      ),
    );
  }

  // Todo: buildEmail
  Widget _buildEmail(PersonController personController) {
    return Container(
      height: 78,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Wrap(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Email", style: TextStyle(color: Colors.black, fontSize: 16),),
              SizedBox(height: 8,),
              SizedBox(
                height: 46,
                child: CustomTextFieldWidget(
                  _emailCtrl,
                  isPhoneNumber: false,
                  label: "Enter Email",
                  personController: personController,
                  onChange: (value){
                    if(_emailCtrl.text.isNotEmpty){
                      personController.setEmail(true);
                    }else {
                      personController.setEmail(false);
                    }
                  },
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  // Todo: buildPhone
  Widget _buildPhone(PersonController personController) {
    return Container(
      height: 78,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Wrap(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Phone Number", style: TextStyle(color: Colors.black, fontSize: 16),),
              SizedBox(height: 8,),
              SizedBox(
                height: 46,
                child: CustomTextFieldWidget(
                  _phoneCtrl,
                  isPhoneNumber: true,
                  label: "Enter Phone Number",
                  personController: personController,
                  onChange: (value){
                    if(_phoneCtrl.text.isNotEmpty){
                      personController.setPhone(true);
                    }else {
                      personController.setPhone(false);
                    }
                  },
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  // Todo: buildSkill
  Widget _buildSkill(PersonController personController) {
    return Container(
      height: 78,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Wrap(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Skill", style: const TextStyle(color: Colors.black, fontSize: 16)),
              SizedBox(height: 8),
              CupertinoButton(
                padding: EdgeInsets.all(0),
                onPressed: (){
                  customBuildButtonSheetCustom(
                    context: context,
                    titleName: "Skill",
                    selectInformationType: SelectInformationType.skill,
                    allItem: skillOther,
                    onText: () {
                      setState(() {
                        _skillCtrl.text = skillOther[skillNumber];
                      });
                      Get.back();
                    },
                  );
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 16, right: 8),
                  height: 46,
                  width: double.infinity,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(4), color: Colors.grey.shade200),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        _skillCtrl.text.isEmpty ? "select" : _skillCtrl.text,
                        style: TextStyle(color: _skillCtrl.text.isEmpty ? Colors.grey : Colors.black87)
                      ),
                      Spacer(),
                      Icon(Icons.arrow_drop_down, color: Colors.grey, size: 28)
                    ],
                  ),
                )
              )
            ],
          ),
        ],
      ),
    );
  }

  // Todo: buildDetailAddress
  Widget _buildAddress(PersonController personController) {
    return Container(
      height: 155,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Wrap(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Address", style: TextStyle(color: Colors.black, fontSize: 16)),
              SizedBox(height: 8,),
              CustomTextFieldWidget(
                _addressCtrl,
                isPhoneNumber: false,
                label: "Enter Address",
                maxLine: 5,
                personController: personController,
                onChange: (value){
                  _addressCtrl.text;
                },
              )
            ],
          ),
        ],
      ),
    );
  }
}
