// ignore_for_file: prefer_const_constructors, avoid_print, must_be_immutable

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:v_tech_coding/data/model/person_model.dart';
import 'package:v_tech_coding/helper/person_helper.dart';
import 'package:v_tech_coding/util/next_screen.dart';
import 'package:v_tech_coding/view/screen/person/crud_person_screen.dart';
import 'package:v_tech_coding/view/screen/person/detail_person_screen.dart';

class PersonScreen extends StatefulWidget {
  const PersonScreen({super.key});

  @override
  State<PersonScreen> createState() => _PersonScreenState();
}

class _PersonScreenState extends State<PersonScreen> {

  final PersonHelper _personHelper = PersonHelper();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return Scaffold(
            appBar: _buildAppBar,
            body: _buildBody,
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  // Todo: buildAppBar
  AppBar get _buildAppBar {
    return AppBar(
      backgroundColor: Colors.orange,
      centerTitle: true,
      title: Text('Person'),
      actions: [
        IconButton(
          onPressed: (){
            nextScreen(context, CrudPersonScreen(isInsert: true));
          },
          icon: Icon(Icons.add_circle_outline),
        ),
      ],
    );
  }

  // Todo: buildBody
  Widget get _buildBody {
    return Container(
      alignment: Alignment.topCenter,
      child: _buildSteamBuilder,
    );
  }

  // Todo: buildSteamBuilder
  StreamBuilder get _buildSteamBuilder {
    return StreamBuilder<QuerySnapshot>(
      stream: _personHelper.getPersonList(),
      builder: (context, snapshot) {
        if(snapshot.hasError) {
          return Text('Something went wrong');
        } else {
          if(snapshot.hasData){
            return _buildListView(snapshot.data!.docs);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        }
      }
    );
  }

  // Todo: buildListView
  Widget _buildListView(List<DocumentSnapshot> documents) {
    List<PersonModel> personList = documents.map((data) => PersonModel.fromSnapshot(data)).toList();
    return ListView.builder(
      shrinkWrap: true,
      itemCount: personList.length,
      itemBuilder: (context, index) {
        return _buildListTile(personList[index]);
      }
    );
  }

  // Todo: buildListTile
  Widget _buildListTile(PersonModel personModel) {
    return Slidable(
      startActionPane: ActionPane(
        motion: const ScrollMotion(),
        extentRatio: 0.45,
        children: [
          SlidableAction(
            onPressed: (context) {
              nextScreen(context, CrudPersonScreen(isInsert: false, personModel: personModel));
            },
            label: 'Update',
            backgroundColor: Colors.green,
            icon: Icons.edit,
          ),
          SlidableAction(
            onPressed: (context) {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Delete'),
                    content: Text('Are you sure want to delete this data?'),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Get.back();
                        },
                        child: Text('Cancel'),
                      ),
                      TextButton(
                        onPressed: () {
                          _personHelper.deletePerson(personModel);
                          Get.back();
                        },
                        child: Text('Delete'),
                      ),
                    ],
                  );
                }
              );
            },
            label: 'Delete',
            backgroundColor: Colors.red,
            icon: Icons.delete,
          ),
        ],
      ),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              nextScreen(context, DetailPersonScreen(personModel: personModel));
            },
            child: ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.orange,
                child: Text(personModel.name?.substring(0, 1) ?? ""),
              ),
              title: Text("${personModel.name ?? ""} (${personModel.skill ?? ""})"),
              subtitle: Text(personModel.email ?? ""),
            ),
          ),
          Container(height: 1, color: Colors.grey, margin: EdgeInsets.symmetric(horizontal: 16)),
        ],
      ),
    );
  }
}
