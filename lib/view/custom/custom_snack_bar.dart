import 'package:flutter/material.dart';
import 'package:v_tech_coding/util/dimension.dart';
import 'package:v_tech_coding/util/style.dart';

void customSnackBar(String message, BuildContext context, {bool isError = true, Color? colors}) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      behavior: SnackBarBehavior.floating,
      content: Text(
        message,
        style: robotoRegular.copyWith(fontSize: Dimension.fontSizeDefault),
      ),
      backgroundColor: colors ?? (isError ? Colors.red : Colors.green),
    ),
  );
}