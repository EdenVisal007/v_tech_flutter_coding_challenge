// ignore_for_file: prefer_const_constructors, must_be_immutable, unnecessary_statements, library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:get/get.dart';

// Todo: SelectInformationType
enum SelectInformationType {
  gender, skill, other
}

int genderNumber = -1;
int skillNumber = -1;
int otherNumber = -1;

// Todo: CustomButtonSheetContentScreen
class CustomButtonSheetContentScreen extends StatefulWidget {

  final String titleName;
  late SelectInformationType selectInformationType;
  late List<dynamic> allItem;
  final VoidCallback onText;

  CustomButtonSheetContentScreen({
    super.key,
    required this.titleName,
    required this.selectInformationType,
    required this.allItem,
    required this.onText,
  });

  @override
  _CustomButtonSheetContentScreenState createState() => _CustomButtonSheetContentScreenState();
}

class _CustomButtonSheetContentScreenState extends State<CustomButtonSheetContentScreen> {

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 16),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              color: Colors.transparent,
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Icon(Icons.check, color: Colors.transparent, size: 40),
            ),
            Text(widget.titleName.tr, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400)),
            GestureDetector(
              onTap: () => Get.back(),
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Image.asset("assets/images/close.png", fit: BoxFit.fill, width: 40, height: 40),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        ListView.builder(
          itemCount: widget.allItem.length,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  switch (widget.selectInformationType) {
                    case SelectInformationType.gender:
                      genderNumber = index;
                      break;
                    case SelectInformationType.skill:
                      skillNumber = index;
                      break;
                    case SelectInformationType.other:
                      otherNumber = index;
                      break;
                  }
                });
                widget.onText();
              },
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 16),
                padding: EdgeInsets.all(16),
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: getCustomBackgroundColorForSelectType(widget.selectInformationType, index),
                ),
                child: Text(
                  widget.allItem[index].toString(),
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: getCustomForegroundColorForSelectType(widget.selectInformationType, index)
                  )
                ),
              ),
            );
          },
        ),
        SizedBox(height: 50),
      ],
    );
  }
}

// Todo: customColorForSelectType
Color getCustomBackgroundColorForSelectType(SelectInformationType selectInformationType, int index) {
  switch (selectInformationType) {
    case SelectInformationType.gender:
      return (index == genderNumber) ? Colors.orange.withOpacity(0.4) : Colors.transparent;
    case SelectInformationType.skill:
      return (index == skillNumber) ? Colors.orange.withOpacity(0.4) : Colors.transparent;
    case SelectInformationType.other:
      return (index == otherNumber) ? Colors.orange.withOpacity(0.4) : Colors.transparent;
  }
}

// Todo: getCustomForeColorForSelectType
Color getCustomForegroundColorForSelectType(SelectInformationType selectInformationType, int index) {
  switch (selectInformationType) {
    case SelectInformationType.gender:
      return (index == genderNumber) ? Colors.orange : Colors.black;
    case SelectInformationType.skill:
      return (index == skillNumber) ? Colors.orange : Colors.black;
    case SelectInformationType.other:
      return (index == otherNumber) ? Colors.orange : Colors.black;
  }
}

// Todo: customBuildButtonSheetCustom
void customBuildButtonSheetCustom({
  required BuildContext context,
  required String titleName,
  String? text,
  SelectInformationType selectInformationType = SelectInformationType.other,
  required List allItem,
  required VoidCallback onText,
}) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
    builder: (BuildContext context) {
      return CustomButtonSheetContentScreen(
        titleName: titleName,
        selectInformationType: selectInformationType,
        allItem: allItem,
        onText: onText,
      );
    },
  );
}