// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:v_tech_coding/controller/person_controller.dart';
import 'package:v_tech_coding/util/dimension.dart';

class CustomTextFieldWidget extends StatefulWidget {

  final TextEditingController textEditingController;
  final String? label;
  final bool isPhoneNumber;
  final bool isPrefix;
  final bool isShowSuffix;
  final bool isClear;
  final bool isObscureText;
  final BorderSide border;
  final int maxLine;
  final Function(String value)? onChange;
  final Function(String)? onSubmitted;
  final Function()? onEditingComplete;
  final Function()? onPress;
  final PersonController? personController;
  const CustomTextFieldWidget(
    this.textEditingController,
    {
      super.key,
      this.label,
      this.isPhoneNumber = true,
      this.isPrefix = false,
      this.isShowSuffix = false,
      this.isClear = false,
      this.isObscureText = false,
      this.border = const BorderSide(color: Colors.transparent),
      this.maxLine = 1,
      this.onChange,
      this.onSubmitted,
      this.onEditingComplete,
      this.onPress,
      this.personController,
    }
  );

  @override
  State<CustomTextFieldWidget> createState() => _CustomTextFieldWidgetState();
}

class _CustomTextFieldWidgetState extends State<CustomTextFieldWidget> {

  @override
  Widget build(BuildContext context) {
    return Form(
      child: TextFormField(
        controller: widget.textEditingController,
        keyboardType: widget.isPhoneNumber? TextInputType.phone : TextInputType.text,
        validator: ((value) {
          widget.isPhoneNumber ? CustomValidatorWidget().validateMobile(value!) : null;
          return null;
        }),
        maxLines: widget.maxLine,
        onChanged: widget.onChange,
        onFieldSubmitted: widget.onSubmitted,
        onEditingComplete: widget.onEditingComplete,
        obscureText: widget.isObscureText,
        textInputAction: TextInputAction.done,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(bottom: 10),
          filled: widget.personController!.isTypingComplete ? false : true,
          fillColor: widget.textEditingController.text.isEmpty ? Colors.grey.shade200 : Colors.grey.shade200,
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          labelStyle: TextStyle(fontSize: 16),
          border: InputBorder.none,
          prefix: widget.isPrefix ? Text("Hello", style: TextStyle(fontSize: 16, color: Colors.transparent)) : Text("  "),
          hintText: widget.label,
          suffix: Text(" "),
          suffixIcon: widget.isShowSuffix ? Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(width: Dimension.paddingSizeExtraSmall),
              IconButton(
                iconSize: 24,
                splashColor: Colors.transparent,
                icon: ImageIcon(AssetImage(widget.isObscureText ? "assets/images/eye.png" : "assets/images/eye_off.png"), color: Colors.grey,),
                onPressed: widget.onPress,
              ),
            ],
          ) : null,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
            borderRadius: BorderRadius.circular(Dimension.paddingSizeExtraSmall),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: widget.border,
            borderRadius: BorderRadius.circular(Dimension.paddingSizeExtraSmall),
          ),
        ),
        cursorColor: Colors.black45,
      ),
    );
  }
}

// Todo: CustomValidatorWidget (for validate mobile number)
class CustomValidatorWidget {
  String validateMobile(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = RegExp(pattern);
    if (value.isEmpty) {
      return 'Please enter mobile number';
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid mobile number';
    }
    return "";
  }
}