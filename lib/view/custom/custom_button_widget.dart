// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class CustomButtonWidget {

  // Todo: buildButtonClick
  static Widget buildButtonClick({required String title, required bool activeColor, bool notTextBold = true, void Function()? onPress}){
    return SizedBox(
      width: double.infinity,
      height: 45,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          side: BorderSide(width: 0, color: Colors.transparent),
          backgroundColor: activeColor ? Colors.orange : Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        ),
        onPressed: onPress,
        child: Text(
          title,
          style: TextStyle(
              color: activeColor ? Colors.white : Colors.grey.shade200,
              fontSize: 18,
              fontWeight: notTextBold ? FontWeight.w300 : FontWeight.w500
          ),
        )
      ),
    );
  }
}