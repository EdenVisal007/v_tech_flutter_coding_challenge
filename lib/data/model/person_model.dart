import 'package:cloud_firestore/cloud_firestore.dart';

class PersonModel {

  static const pName = 'name';
  static const pGender = 'gender';
  static const pEmail = 'email';
  static const pPhone = 'phone';
  static const pAddress = 'address';
  static const pSkill = 'skill';
  static const pPerson = 'person';

  String? name;
  String? gender;
  String? email;
  String? phone;
  String? address;
  String? skill;
  DocumentReference? reference;

  PersonModel({
    this.name,
    this.gender,
    this.email,
    this.phone,
    this.address,
    this.skill,
    this.reference,
  });

  PersonModel.fromMap(Object? object, {required this.reference}) {
    Map<String, dynamic> map = object as Map<String, dynamic>;
    name = map[pName];
    gender = map[pGender];
    email = map[pEmail];
    phone = map[pPhone];
    address = map[pAddress];
    skill = map[pSkill];
  }

  PersonModel.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data() as Map<String, dynamic>, reference: snapshot.reference);

  Map<String, dynamic> toMap() {
    return {
      pName: name,
      pGender: gender,
      pEmail: email,
      pPhone: phone,
      pAddress: address,
      pSkill: skill,
    };
  }
}