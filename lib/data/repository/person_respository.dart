// ignore_for_file: prefer_final_fields

import 'package:shared_preferences/shared_preferences.dart';
import 'package:v_tech_coding/data/dio/api_client.dart';

class PersonRepository {
  final DioClient dioClient;
  final SharedPreferences sharedPreferences;

  PersonRepository({required this.dioClient, required this.sharedPreferences});
}
